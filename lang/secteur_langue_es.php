<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/secteur_langue?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'avis_rubrique_source' => 'Debes traducir primero las secciones que contienen está sección',

	// S
	'secteur_langue_titre' => 'Sector por idioma'
);
