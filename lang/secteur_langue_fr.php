<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/secteur_langue.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'avis_rubrique_source' => 'Vous devez d’abord traduire toutes les rubriques parentes',

	// S
	'secteur_langue_titre' => 'Secteur par langue'
);
