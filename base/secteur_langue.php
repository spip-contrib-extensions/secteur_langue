<?php
/**
 * Déclarations relatives à la base de données
 *
 * @plugin     Secteur par langue
 * @copyright  2019 - 2022
 * @author     Rainer Müller
 * @licence    GNU/GPL v3
 * @package    SPIP\Secteur_langue\Base
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ajouter id_trad à la table rubriques
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des objets editoriaux
 * @return array
 *     Description des objets editoriaux
 */
function secteur_langue_declarer_tables_objets_sql($tables) {
	// Extension de la table rubriques
	$tables['spip_rubriques']['field']['id_trad'] = "bigint(21) DEFAULT 0";
	return $tables;
}
