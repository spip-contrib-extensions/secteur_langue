<?php

/**
 * @file
 * Utilisations de pipelines par Secteur par langue.
 *
 * @plugin     Secteur par langue
 * @copyright  2019 - 2022
 * @author     Rainer Müller
 * @licence    GNU/GPL v3
 * @package    SPIP\Secteur_langue\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ajouter des contenus dans la partie <head> des pages de l’espace privé.
 *
 * @param array $flux
 *   Données du pipeline.
 *
 * @return array
 *   Données du pipelin.
 */
function secteur_langue_header_prive($flux) {
	$flux .='
	<script type="text/javascript">
		$(document).ready(function() {
			$(".avis_source").click( function() {
				javascript:alert("'._T('secteur_langue:avis_rubrique_source').'");
			});
		});
	</script>
	';
	return $flux;
}

/**
 * Modifier le tableau retourné par la fonction traiter d’un formulaire CVT ou effectuer des traitements supplémentaires.
 *
 * @param array $flux
 *   Les données du pipeline
 *
 * @return array
 *   Les données du pipeleine.
 */
function secteur_langue_formulaire_traiter($flux) {
	$form= $flux['args']['form'];
	// Assurer que la langue enregistré soit celle de la lang_dest, ne fonctionnait plus automatiquement sous spip 4.
	if ($form == 'editer_rubrique' AND $lang = _request('lang_dest')) {
		sql_updateq('spip_rubriques', ['lang' => $lang, 'langue_choisie' => 'oui'], 'id_rubrique=' . $flux['data']['id_rubrique']);
	}

	// Sous spip 4 il faut vérifier que l'article ait bien la lngue de sa rubrique.
	if ($form == 'editer_article') {
		$id_article = $flux['data']['id_article'];
		$parent = sql_fetsel('r.id_rubrique,r.lang AS lang_rub, a.lang AS lang_art', 'spip_articles a JOIN spip_rubriques r ON a.id_rubrique = r.id_rubrique', 'id_article=' . $id_article);

		if ($parent['lang_rub'] != $parent['lang_art']) {
			sql_updateq('spip_articles', ['lang' => $parent['lang_rub'], 'langue_choisie' => 'oui'], 'id_article=' .  $id_article);
		}

	}

	return $flux;
}

